﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_OrdenTrabajo" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_OrdenTrabajo {
        [OperationContract]
        ResultadoDTO CrearOT(int idMaquina, int idCreador, int idEncargado, int idStatus );
        
        [OperationContract]
        ResultadoDTO CambiarEstadoOT(int id, int idStatus);
        [OperationContract]
        OrdenTrabajoSRV BuscarOrdenTrabajo(int idOT);
        [OperationContract]
        List<OrdenTrabajoSRV> ListarOrdenTrabajo();
    }

    public class OrdenTrabajoSRV {
        [DataMember]
        public int idOT { get; set; }
        [DataMember]
        public DateTime fechaCreacion { get; set; }
        [DataMember]
        public int idMaquina { get; set; }
        [DataMember]
        public String nombreMaquina { get; set; }
        [DataMember]
        public int idCreador { get; set; }
        [DataMember]
        public String nombreCreador { get; set; }
        [DataMember]
        public int idEncargado { get; set; }
        [DataMember]
        public String nombreEncargado { get; set; }
        [DataMember]
        public int idStatusOT { get; set; }
        [DataMember]
        public String estadoOT { get; set; }
        [DataMember]
        public DateTime? fechaCierre { get; set; }
        [DataMember]
        public List<DetalleOTSRV> listaDetalles { get; set; }
    }
}
