﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Login" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Login {
        [OperationContract]
        UsuarioSRV IniciaSesion(string usuario, string clave);
        [OperationContract]
        Boolean validaLogin(String user);
    }
    [DataContract]
    public class UsuarioSRV {
        [DataMember]
        public Int32 idPersona { get; set; }
        [DataMember]
        public string NombrePersona { get; set; }
        [DataMember]
        public string Passwd { get; set; }
        [DataMember]
        public string CargoPersona { get; set; }
    }
}
