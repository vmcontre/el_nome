﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;


namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_StatusOT" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_StatusOT.svc o SRV_StatusOT.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_StatusOT : ISRV_StatusOT {
        StatusOTNeg objStatusOTNeg = new StatusOTNeg();

        public Int32 BuscarIDStatusOT(String descrStatus) {
            return objStatusOTNeg.BuscarIDStatusOT(descrStatus);
        }

        public String BuscarStatusOT(Int32 id) {
            return objStatusOTNeg.BuscarStatusOT(id);
        }

        public List<StatusOTSRV> listarStatusOT() {
            List<StatusOTSRV> listaStatusOTSVR = new List<StatusOTSRV>();
            List<ItemStatusOT> listaItemStatusOT = objStatusOTNeg.listarStatusOT();

            foreach (ItemStatusOT i in listaItemStatusOT) {
                StatusOTSRV objTemp = new StatusOTSRV() {
                    DescrStatus = i.descrStatus,
                    idStatusOT = i.idStatusOT
                };
                listaStatusOTSVR.Add(objTemp);
            }
            return listaStatusOTSVR;
        }
    }
}
