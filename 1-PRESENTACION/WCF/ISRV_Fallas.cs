﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISRV_Fallas" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISRV_Fallas {
        [OperationContract]
        ResultadoDTO CrearFalla(String nombre, string descripcion, string sugerencia);
        [OperationContract]
        List<FallasSRV> ListarFallas();
        ResultadoDTO ModificarFallas(int idFalla, String descripcion, String sugerencia);
        FallasSRV BuscarFalla(int idFalla);

    }

    public class FallasSRV {
        [DataMember]
        public int idFalla { get; set; }
        [DataMember]
        public String nombreFalla { get; set; }
        [DataMember]
        public String descrFalla { get; set; }
        [DataMember]
        public String sugerenciaFalla { get; set; }
    }
}
