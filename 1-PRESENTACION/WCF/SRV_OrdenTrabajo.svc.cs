﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Negocio;

namespace WCF {
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SRV_OrdenTrabajo" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione SRV_OrdenTrabajo.svc o SRV_OrdenTrabajo.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class SRV_OrdenTrabajo : ISRV_OrdenTrabajo {

        OrdenTrabajoNeg objOrdenTrabajoNeg = new OrdenTrabajoNeg();

        public OrdenTrabajoSRV BuscarOrdenTrabajo(Int32 idOT) {
            OrdenTrabajoDTO objOT = new OrdenTrabajoDTO();
            OrdenTrabajoSRV objOTSRV = new OrdenTrabajoSRV();
            List<DetalleOTSRV> listDetTemp = new List<DetalleOTSRV>();

            objOT = objOrdenTrabajoNeg.BuscarOrdenTrabajo(idOT);
            List<DetalleOTDTO> objListaDetDTO = new List<DetalleOTDTO>();

            if (objOT != null) {
                objOTSRV.idOT = objOT.idOT;
                objOTSRV.nombreCreador = objOT.nombreCreador;
                objOTSRV.nombreEncargado = objOT.nombreEncargado;
                objOTSRV.nombreMaquina = objOT.nombreMaquina;
                objOTSRV.estadoOT = objOT.status;
                objOTSRV.idStatusOT = objOT.idStatusOT;
                //aqui va un for each para cambiar el tipo de variable

                objListaDetDTO = objOT.listaDetalles;
                foreach (DetalleOTDTO d in objListaDetDTO) {
                    DetalleOTSRV objDetallesSvrRet = new DetalleOTSRV() {
                        nombreTecnico = d.nombreTecnico,
                        nombreFalla = d.nombreFalla,
                        idDOT = d.idDOT,
                        idOT = d.idOT,
                        fecha = d.fecha,
                    };
                    listDetTemp.Add(objDetallesSvrRet);
                }
                objOTSRV.listaDetalles = listDetTemp;
                objOTSRV.fechaCierre = objOT.fechaCierre;
                objOTSRV.fechaCreacion = objOT.fechaCreacion;
            }

            return objOTSRV;
        }

        public ResultadoDTO CambiarEstadoOT(Int32 id, Int32 idStatus) {
            return objOrdenTrabajoNeg.CambiarEstadoOT(id, idStatus);
        }

        public ResultadoDTO CrearOT(Int32 idMaquina, Int32 idCreador, Int32 idEncargado, Int32 idStatus) {
            ResultadoDTO r = null;
            ordenTrabajo objOT = new ordenTrabajo();
            objOT.fechaCreacion = DateTime.Now;
            objOT.idMaquina = idMaquina;
            objOT.idCreador = idCreador;
            objOT.idEncargado = idEncargado;
            objOT.idStatusOT = idStatus;

            r = objOrdenTrabajoNeg.CrearOT(objOT);

            return r;
        }

        public List<OrdenTrabajoSRV> ListarOrdenTrabajo() {
            List<Int32> listaNroOT = objOrdenTrabajoNeg.ListarNroOT();
            List<OrdenTrabajoSRV> ListaOTSVR = new List<OrdenTrabajoSRV>();
            foreach (Int32 i in listaNroOT) {
                OrdenTrabajoSRV objTemp = BuscarOrdenTrabajo(i);
                ListaOTSVR.Add(objTemp);
            }
            return ListaOTSVR;
        }

    }

    
}
