﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB {
    public partial class login : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

        }

        protected void btnLogin_Click(Object sender, EventArgs e) {
            string user = login_username.Text;
            string passwd = login_password.Text;

            SRV_Login.UsuarioSRV objUsuario = new SRV_Login.UsuarioSRV();
            SRV_Login.SRV_LoginClient objLoginClient = new SRV_Login.SRV_LoginClient();

            if (objLoginClient.validaLogin(user)) {
                objUsuario = objLoginClient.IniciaSesion(user, passwd);
                if (objUsuario != null) {
                    Session["sesionUsuario"] = objUsuario;
                    Response.Redirect("~/admin/index.aspx");
                } else {
                    //aqui va la alerta super pulenta del manu
                    lblResultadoLogin.Text = "usuario o contraseña incorrecta";
                    login_username.Text = "";
                    login_password.Text = "";
                }
            } else {
                lblResultadoLogin.Text = "usuario deshabilitado o inexistente";
                login_username.Text = "";
                login_password.Text = "";
            }
        }
    }
}