﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WEB.SRV_DetOT {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ResultadoDTO", Namespace="http://schemas.datacontract.org/2004/07/Entidades")]
    [System.SerializableAttribute()]
    public partial class ResultadoDTO : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool errorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<int> idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string mensajeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool error {
            get {
                return this.errorField;
            }
            set {
                if ((this.errorField.Equals(value) != true)) {
                    this.errorField = value;
                    this.RaisePropertyChanged("error");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<int> id {
            get {
                return this.idField;
            }
            set {
                if ((this.idField.Equals(value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string mensaje {
            get {
                return this.mensajeField;
            }
            set {
                if ((object.ReferenceEquals(this.mensajeField, value) != true)) {
                    this.mensajeField = value;
                    this.RaisePropertyChanged("mensaje");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DetalleOTSRV", Namespace="http://schemas.datacontract.org/2004/07/WCF")]
    [System.SerializableAttribute()]
    public partial class DetalleOTSRV : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescRepuestoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime fechaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int idDOTField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int idFallaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int idOTField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int idRepuestoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int idTecnicoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nombreFallaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string nombreTecnicoField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DescRepuesto {
            get {
                return this.DescRepuestoField;
            }
            set {
                if ((object.ReferenceEquals(this.DescRepuestoField, value) != true)) {
                    this.DescRepuestoField = value;
                    this.RaisePropertyChanged("DescRepuesto");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime fecha {
            get {
                return this.fechaField;
            }
            set {
                if ((this.fechaField.Equals(value) != true)) {
                    this.fechaField = value;
                    this.RaisePropertyChanged("fecha");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int idDOT {
            get {
                return this.idDOTField;
            }
            set {
                if ((this.idDOTField.Equals(value) != true)) {
                    this.idDOTField = value;
                    this.RaisePropertyChanged("idDOT");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int idFalla {
            get {
                return this.idFallaField;
            }
            set {
                if ((this.idFallaField.Equals(value) != true)) {
                    this.idFallaField = value;
                    this.RaisePropertyChanged("idFalla");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int idOT {
            get {
                return this.idOTField;
            }
            set {
                if ((this.idOTField.Equals(value) != true)) {
                    this.idOTField = value;
                    this.RaisePropertyChanged("idOT");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int idRepuesto {
            get {
                return this.idRepuestoField;
            }
            set {
                if ((this.idRepuestoField.Equals(value) != true)) {
                    this.idRepuestoField = value;
                    this.RaisePropertyChanged("idRepuesto");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int idTecnico {
            get {
                return this.idTecnicoField;
            }
            set {
                if ((this.idTecnicoField.Equals(value) != true)) {
                    this.idTecnicoField = value;
                    this.RaisePropertyChanged("idTecnico");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string nombreFalla {
            get {
                return this.nombreFallaField;
            }
            set {
                if ((object.ReferenceEquals(this.nombreFallaField, value) != true)) {
                    this.nombreFallaField = value;
                    this.RaisePropertyChanged("nombreFalla");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string nombreTecnico {
            get {
                return this.nombreTecnicoField;
            }
            set {
                if ((object.ReferenceEquals(this.nombreTecnicoField, value) != true)) {
                    this.nombreTecnicoField = value;
                    this.RaisePropertyChanged("nombreTecnico");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SRV_DetOT.ISRV_DetOT")]
    public interface ISRV_DetOT {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/CrearDetalleOT", ReplyAction="http://tempuri.org/ISRV_DetOT/CrearDetalleOTResponse")]
        WEB.SRV_DetOT.ResultadoDTO CrearDetalleOT(int idOT, int idTec, int idRep, int idFalla);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/CrearDetalleOT", ReplyAction="http://tempuri.org/ISRV_DetOT/CrearDetalleOTResponse")]
        System.Threading.Tasks.Task<WEB.SRV_DetOT.ResultadoDTO> CrearDetalleOTAsync(int idOT, int idTec, int idRep, int idFalla);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/ListarDOT", ReplyAction="http://tempuri.org/ISRV_DetOT/ListarDOTResponse")]
        WEB.SRV_DetOT.DetalleOTSRV[] ListarDOT(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/ListarDOT", ReplyAction="http://tempuri.org/ISRV_DetOT/ListarDOTResponse")]
        System.Threading.Tasks.Task<WEB.SRV_DetOT.DetalleOTSRV[]> ListarDOTAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/BuscarDetalleOT", ReplyAction="http://tempuri.org/ISRV_DetOT/BuscarDetalleOTResponse")]
        WEB.SRV_DetOT.DetalleOTSRV BuscarDetalleOT(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/BuscarDetalleOT", ReplyAction="http://tempuri.org/ISRV_DetOT/BuscarDetalleOTResponse")]
        System.Threading.Tasks.Task<WEB.SRV_DetOT.DetalleOTSRV> BuscarDetalleOTAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/ModificarDetOT", ReplyAction="http://tempuri.org/ISRV_DetOT/ModificarDetOTResponse")]
        WEB.SRV_DetOT.ResultadoDTO ModificarDetOT(int id, int idTec, int idRep, int idFalla);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISRV_DetOT/ModificarDetOT", ReplyAction="http://tempuri.org/ISRV_DetOT/ModificarDetOTResponse")]
        System.Threading.Tasks.Task<WEB.SRV_DetOT.ResultadoDTO> ModificarDetOTAsync(int id, int idTec, int idRep, int idFalla);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISRV_DetOTChannel : WEB.SRV_DetOT.ISRV_DetOT, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SRV_DetOTClient : System.ServiceModel.ClientBase<WEB.SRV_DetOT.ISRV_DetOT>, WEB.SRV_DetOT.ISRV_DetOT {
        
        public SRV_DetOTClient() {
        }
        
        public SRV_DetOTClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SRV_DetOTClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SRV_DetOTClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SRV_DetOTClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public WEB.SRV_DetOT.ResultadoDTO CrearDetalleOT(int idOT, int idTec, int idRep, int idFalla) {
            return base.Channel.CrearDetalleOT(idOT, idTec, idRep, idFalla);
        }
        
        public System.Threading.Tasks.Task<WEB.SRV_DetOT.ResultadoDTO> CrearDetalleOTAsync(int idOT, int idTec, int idRep, int idFalla) {
            return base.Channel.CrearDetalleOTAsync(idOT, idTec, idRep, idFalla);
        }
        
        public WEB.SRV_DetOT.DetalleOTSRV[] ListarDOT(int id) {
            return base.Channel.ListarDOT(id);
        }
        
        public System.Threading.Tasks.Task<WEB.SRV_DetOT.DetalleOTSRV[]> ListarDOTAsync(int id) {
            return base.Channel.ListarDOTAsync(id);
        }
        
        public WEB.SRV_DetOT.DetalleOTSRV BuscarDetalleOT(int id) {
            return base.Channel.BuscarDetalleOT(id);
        }
        
        public System.Threading.Tasks.Task<WEB.SRV_DetOT.DetalleOTSRV> BuscarDetalleOTAsync(int id) {
            return base.Channel.BuscarDetalleOTAsync(id);
        }
        
        public WEB.SRV_DetOT.ResultadoDTO ModificarDetOT(int id, int idTec, int idRep, int idFalla) {
            return base.Channel.ModificarDetOT(id, idTec, idRep, idFalla);
        }
        
        public System.Threading.Tasks.Task<WEB.SRV_DetOT.ResultadoDTO> ModificarDetOTAsync(int id, int idTec, int idRep, int idFalla) {
            return base.Channel.ModificarDetOTAsync(id, idTec, idRep, idFalla);
        }
    }
}
