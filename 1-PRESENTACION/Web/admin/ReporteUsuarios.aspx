﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteUsuarios.aspx.cs" Inherits="WEB.admin.ReporteUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-body">
        <!--#include file="MenuLateral.html"-->
        <main class="overflow-hidden">
            <header class="page header">
                <div class="content">
                    <h1 class="display-4 mb-0">Usuarios Registrados</h1>
                </div>
            </header>
            <div class="content">
                <p class="lead text-muted"> <asp:Label ID="lblTitulo" runat="server" Text="Agregar nuevo Usuario"></asp:Label></p>
                <p class="lead text-muted">
                    <asp:Label ID="lblResultadoAgregar" runat="server" Text=""></asp:Label>
                </p>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblNuevoUsuario" runat="server" Text="Nombre:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtNuevoUsuario" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblCargo" runat="server" Text="Cargo:" CssClass="form-label"></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlCargo" CssClass="form-control" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:Label ID="lblNuevoUserName" runat="server" Text="Nombre de Usuario:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtNuevoUserName" runat="server" CssClass="form-control"></asp:TextBox>
                        <p class="lead text-muted">
                            <asp:Label ID="lblUsuarioExistente" runat="server" Text="" CssClass="form-label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdfModificar" />
                            <asp:HiddenField runat="server" ID="hdfUserNameActual" />
                            <asp:HiddenField runat="server" ID="hdfId2" />
                            <asp:HiddenField runat="server" ID="hdfId" />
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <asp:Label ID="lblNuevaContrasena" runat="server" Text="Contraseña:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtNuevaContrasena" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblcontrasena2" runat="server" Text="Repita Contraseña:" CssClass="form-label"></asp:Label>
                        <asp:TextBox ID="txtNuevaContrasena2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="lead text-muted">
                            <asp:Label ID="lblContrasenasIguales" runat="server" Text="" CssClass="form-label"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <br />
                        <br />
                        <asp:Button ID="btnAgregarUsuario" runat="server" Text="Agregar" CssClass="btn btn-primary" OnClick="btnAgregarUsuario_Click" />
                    </div>
                    <div class="col-md-3">
                        <br />
                        <br />
                        <asp:Button ID="btnCambiarEstado" runat="server" Text="Cambiar Estado" CssClass="btn btn-primary" OnClick="btnCambiarEstado_Click"  />
                    </div>
                </div>
            </div>

            <div class="content">
                
                <div class="table-responsive">

                    <asp:GridView ID="gvUsuarios" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-clean" 
                        OnRowCreated="gvUsuarios_RowCreated" 
                        OnRowDataBound="gvUsuarios_RowDataBound">
                        <HeaderStyle CssClass="table-clean" />
                        <Columns>
                            <asp:BoundField DataField="idPersona" HeaderText="ID" />
                            <asp:BoundField DataField="nombrePersona" HeaderText="Nombre Completo" />
                            <asp:BoundField DataField="userName" HeaderText="Usuario" />
                            <asp:BoundField DataField="nombreCargo" HeaderText="Cargo" />
                            <asp:BoundField DataField="activo" HeaderText="Activo" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            
        </main>
    </div>
</asp:Content>
