﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB.admin {
    public partial class CrearOT : System.Web.UI.Page {
        SRV_Login.UsuarioSRV objUsuarioConectado = new SRV_Login.UsuarioSRV();
        SRV_Maquinas.SRV_MaquinasClient maquinasClient = new SRV_Maquinas.SRV_MaquinasClient();
        SRV_Personas.SRV_PersonasClient personasClient = new SRV_Personas.SRV_PersonasClient();
        SRV_Fallas.SRV_FallasClient fallasClient = new SRV_Fallas.SRV_FallasClient();
        SRV_Repuestos.SRV_RepuestosClient repuestosClient = new SRV_Repuestos.SRV_RepuestosClient();
        SRV_StatusOT.SRV_StatusOTClient statusOTClient = new SRV_StatusOT.SRV_StatusOTClient();


        protected void Page_Load(object sender, EventArgs e) {
            if (!IsPostBack) {
                refrescador();
            }
        }

        protected void btnGuardar_Click(Object sender, EventArgs e) {
            //accede a las funciones
            SRV_OrdenTrabajo.SRV_OrdenTrabajoClient ordenTrabajoClient = new SRV_OrdenTrabajo.SRV_OrdenTrabajoClient();
            SRV_DetOT.SRV_DetOTClient detOTClient = new SRV_DetOT.SRV_DetOTClient();

            if (validador()) {
                try {
                    //OT ->  fechaCreacion, idmaquina, idcreador, idencargado, idstatusot, fechacierre (idOT)
                    //DOT -> idOT, idTecnico, idRepuesto, idFalla, fecha (idDOT)

                    objUsuarioConectado = (SRV_Login.UsuarioSRV)Session["sesionUsuario"];

                    var resNvaOT = ordenTrabajoClient.CrearOT(
                                                        Int32.Parse(ddlMaquina.SelectedValue),
                                                        objUsuarioConectado.idPersona,
                                                        Int32.Parse(ddlEncargado.SelectedValue),
                                                        statusOTClient.BuscarIDStatusOT("Creada"));
                    if ((resNvaOT != null) && (resNvaOT.id > 0)) {
                        //creo el detalle ot
                        var resDetOT = detOTClient.CrearDetalleOT(
                                            (int)resNvaOT.id,
                                            Int32.Parse(ddlEncargado.SelectedValue),
                                            Int32.Parse(ddlRepuesto.SelectedValue),
                                            Int32.Parse(ddlFalla.SelectedValue));
                        if (!resDetOT.error) {
                            refrescador();
                            Response.Redirect("~/admin/index.aspx");
                        }
                    } else {
                        //error, no pude guardar
                    }
                } catch {
                    refrescador();
                    Response.Redirect("~/admin/index.aspx");
                }

            }
        }

        protected void ddlMaquina_SelectedIndexChanged(Object sender, EventArgs e) {

        }

        protected void refrescador() {
            //grilla
            //cargar datos en ddl

            //maquinas
            ddlMaquina.DataSource = maquinasClient.ListarMaquinasActivas();
            ddlMaquina.DataTextField = "descrMaquina";
            ddlMaquina.DataValueField = "idMaquina";
            ddlMaquina.DataBind();
            ddlMaquina.Items.Insert(0, new ListItem(""));

            //Encargado
            ddlEncargado.DataSource = personasClient.ListarPersonasActivas();
            ddlEncargado.DataTextField = "nombrePersona";
            ddlEncargado.DataValueField = "idPersona";
            ddlEncargado.DataBind();
            ddlEncargado.Items.Insert(0, new ListItem(""));

            //Fallas
            ddlFalla.DataSource = fallasClient.ListarFallas();
            ddlFalla.DataTextField = "descrFalla";
            ddlFalla.DataValueField = "idFalla";
            ddlFalla.DataBind();
            ddlFalla.Items.Insert(0, new ListItem(""));

            //Repuestos
            ddlRepuesto.DataSource = repuestosClient.ListarRepuestosConStock();
            ddlRepuesto.DataTextField = "descrRepuesto";
            ddlRepuesto.DataValueField = "idRepuesto";
            ddlRepuesto.DataBind();
            ddlRepuesto.Items.Insert(0, new ListItem(""));
        }

        protected Boolean validador() {

            if (ddlMaquina.Text.Equals("")) return false;
            if (ddlEncargado.Text.Equals("")) return false;
            if (ddlFalla.Text.Equals("")) return false;
            if (ddlRepuesto.Text.Equals("")) {
                return false;
            } else {
                return true;
            }
        }

    }
}