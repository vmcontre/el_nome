﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB {
    public partial class SiteMaster : MasterPage {
        protected void Page_Load(object sender, EventArgs e) {
            if (Session["sesionUsuario"] == null) {
                Response.Redirect("~/login.aspx");
            } else {
                SRV_Login.UsuarioSRV objUsuarioConectado = (SRV_Login.UsuarioSRV)Session["sesionUsuario"];
                NombrePersona.Text = objUsuarioConectado.NombrePersona;
                CargoPersona.Text = objUsuarioConectado.CargoPersona;
            }
        }

        protected void btnLogout_Click(Object sender, EventArgs e) {
            Session["sesionUsuario"] = null;
            Response.Redirect("~/login.aspx");
        }
    }
}