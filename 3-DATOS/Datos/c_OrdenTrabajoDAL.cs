﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
 

namespace Datos
{
    public class OrdenTrabajoDAL {
        public ResultadoDTO CrearOT(ordenTrabajo dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.ordenTrabajo.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idOT;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemOT> ListarOT() {
            List<ItemOT> lista = new List<ItemOT>();
            {
                using (nomeEntities dbo = new nomeEntities()) {
                    lista = (from l in dbo.ordenTrabajo select new ItemOT { idOT = l.idOT, idCreador = l.idCreador, idEncargado = l.idEncargado, idMaquina = l.idMaquina, idStatusOT = l.idStatusOT, fechaCierre = l.fechaCierre, fechaCreacion = l.fechaCreacion }).ToList();
                }
                return lista;
            }
        }

        public ordenTrabajo BuscarOT(int id) {
            ordenTrabajo objOT = new ordenTrabajo();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objOT = dbo.ordenTrabajo.FirstOrDefault(u => u.idOT == id);
                }
            } catch  {
                objOT = null;
            }
            return objOT;
        }

        public ResultadoDTO CambiarEstadoOT(int id, int idStatus) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    ordenTrabajo objOT = dbo.ordenTrabajo.FirstOrDefault(u => u.idOT == id);

                    if (objOT != null) {
                        if ((idStatus == 4) || (idStatus == 5)){
                            objOT.fechaCierre = DateTime.Now;
                            objOT.idStatusOT = idStatus;
                        } else {
                            objOT.idStatusOT = idStatus;
                        }
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = id;
                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }


        public OrdenTrabajoDTO BuscarOrdenTrabajo(int idOT) {
            OrdenTrabajoDTO objOT = new OrdenTrabajoDTO();
            using (nomeEntities dbo = new nomeEntities()) {
                objOT = (from o in dbo.ordenTrabajo where o.idOT == idOT select new OrdenTrabajoDTO {
                    idOT = o.idOT,
                    nombreCreador = o.personas.nombrePersona,
                    nombreEncargado = o.personas1.nombrePersona,
                    nombreMaquina = o.maquinas.descrMaquina,
                    idStatusOT = o.idStatusOT,
                    status = o.statusOT.DescrStatus,
                    fechaCreacion = o.fechaCreacion,
                    fechaCierre = o.fechaCierre,
                    listaDetalles = (from l in o.detalleOT select new DetalleOTDTO {
                        fecha = l.fecha,
                        idDOT = l.idDOT,
                        idOT =l.idOT,
                        nombreFalla =l.fallas.nombreFalla,
                        nombreTecnico =l.personas.nombrePersona
                    }).ToList()
                }).FirstOrDefault();
            }
                return objOT;
        }

        public List<OrdenTrabajoDTO> ListarOrdenTrabajo() {
            List<OrdenTrabajoDTO> listaOTs = new List<OrdenTrabajoDTO>();
            using (nomeEntities dbo = new nomeEntities()) {
                listaOTs = (from o in dbo.ordenTrabajo
                         select new OrdenTrabajoDTO {
                             idOT = o.idOT,
                             nombreCreador = o.personas.nombrePersona,
                             nombreEncargado = o.personas1.nombrePersona,
                             nombreMaquina = o.maquinas.descrMaquina,
                             status = o.statusOT.DescrStatus,
                             fechaCreacion = o.fechaCreacion,
                             fechaCierre = o.fechaCierre,
                             listaDetalles = (from l in o.detalleOT
                                              select new DetalleOTDTO {
                                                  fecha = l.fecha,
                                                  idDOT = l.idDOT,
                                                  idOT = l.idOT,
                                                  nombreFalla = l.fallas.nombreFalla,
                                                  nombreTecnico = l.personas.nombrePersona
                                              }).ToList()
                         }).ToList();
            }
            return listaOTs;
        }

        public List<Int32> ListarNroOT() {
            List<Int32> objLista = new List<int>();
            using (nomeEntities dbo = new nomeEntities()) {
                objLista = (from l in dbo.ordenTrabajo select l.idOT).ToList();
            }
            return objLista;
        }
    }
}
