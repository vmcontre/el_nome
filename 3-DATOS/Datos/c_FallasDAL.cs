﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos {
    public class FallasDAL {
        public ResultadoDTO CrearFalla(fallas dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.fallas.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idFalla;

                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemFallas> ListarFallas() {
            List<ItemFallas> lista = new List<ItemFallas>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.fallas select new ItemFallas { idFalla = l.idFalla, descrFalla =l.descrFalla, nombreFalla = l.nombreFalla, sugerenciaFalla=l.sugerenciaFalla
                }).ToList();
            }
            return lista;
        }

        public ResultadoDTO ModificarFallas(int idFalla, String descripcion, String sugerencia) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    fallas objTemp = dbo.fallas.FirstOrDefault(u => u.idFalla == idFalla);
                    if (objTemp != null) {
                        if ((descripcion != null) && (descripcion != "")) { objTemp.descrFalla = descripcion; }
                        if ((sugerencia != null) && (sugerencia != "")) { objTemp.sugerenciaFalla = sugerencia; }

                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idFalla;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }

                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public fallas BuscarFalla(int idFalla) {
            fallas objFalla = new fallas();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objFalla = dbo.fallas.FirstOrDefault(u => u.idFalla == idFalla);
                }
            } catch  {
                objFalla = null;
            }
            return objFalla;
        }

    }
}
