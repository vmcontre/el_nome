﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos {
    public class PersonasDAL {
        public ResultadoDTO CrearPersona(personas dto) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    dbo.personas.Add(dto);
                    dbo.SaveChanges();
                    r.error = false;
                    r.id = dto.idPersona;
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public ResultadoDTO CambiaEstadoPersona(int idPersona) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    personas objTemp = dbo.personas.FirstOrDefault(u => u.idPersona == idPersona);
                    if (objTemp != null) {
                        objTemp.activo = !objTemp.activo;
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idPersona;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public ResultadoDTO ModificarPersona(int idPersona, String nombrePersona, String UserName, int idCargo) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    personas objTemp = dbo.personas.FirstOrDefault(u => u.idPersona == idPersona);
                    if (objTemp != null) {
                        if ((nombrePersona != null) && (nombrePersona != "")) {
                            objTemp.nombrePersona = nombrePersona;
                        }
                        if (idCargo > 0) {
                            objTemp.idCargo = idCargo;
                        }
                        if ((UserName != null) && (UserName != "")) {
                            if (!ExisteUserName(UserName)) {
                                objTemp.userName = UserName;
                            }
                        }
                        dbo.SaveChanges();
                        r.error = false;
                        r.id = idPersona;

                    } else {
                        r.error = true;
                        r.mensaje = "No se pudo completar actualizacion";
                    }

                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public List<ItemPersonas> ListarPersonas() {
            List<ItemPersonas> lista = new List<ItemPersonas>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.personas
                         select new ItemPersonas {
                             idPersona = l.idPersona,
                             activo = l.activo,
                             idCargo = l.idCargo,
                             nombreCargo = l.cargos.descrCargo,
                             username = l.userName,
                             passwd= l.passwd,
                             nombrePersona = l.nombrePersona
                         }).ToList();
            }
            return lista;
        }

        public Boolean ValidaLogin(String username) {

            personas usuario = new personas();
            using (nomeEntities dbo = new nomeEntities()) {
                usuario = dbo.personas.FirstOrDefault(u => u.userName == username);
                if (usuario != null)  {
                    if (usuario.activo) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        public ItemPersonas Buscar(string user, string passwd) {
            ItemPersonas objPersona = new ItemPersonas();
            using (nomeEntities dbo = new nomeEntities()) {
                personas objtemp = dbo.personas.FirstOrDefault(u => u.userName == user
                    && u.passwd == passwd);
                if (objtemp != null) {
                    objPersona.nombrePersona = objtemp.nombrePersona;
                    objPersona.nombreCargo = objtemp.cargos.descrCargo;
                    objPersona.idPersona = objtemp.idPersona;
                } else {
                    objPersona = null;
                }
            }
            return objPersona;
        }

        public ItemPersonas BuscarID(int idUsuario) {
            ItemPersonas objPersonaRet = new ItemPersonas();
            using (nomeEntities dbo = new nomeEntities()) {
                objPersonaRet = (from p in dbo.personas where p.idPersona==idUsuario
                                 select new ItemPersonas {
                                     idCargo = p.idCargo,
                                     activo = p.activo,
                                     idPersona = p.idPersona,
                                     nombreCargo = p.cargos.descrCargo,
                                     nombrePersona = p.nombrePersona,
                                     passwd = p.passwd,
                                     username = p.userName
                                 }).FirstOrDefault();
            }
            return objPersonaRet;
        }

        public ResultadoDTO CambiarContraseña(int idPersona, String nuevaPasswd) {
            ResultadoDTO r = new ResultadoDTO();
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    personas objTemp = dbo.personas.FirstOrDefault(u => u.idPersona == idPersona);
                    if ((objTemp != null)  && (nuevaPasswd != "")){
                        objTemp.passwd = nuevaPasswd;
                        dbo.SaveChanges();

                    } else {
                        r.error = true;
                        r.mensaje = "no se completó el cambio";
                    }
                }
            } catch (Exception e) {
                r.error = true;
                r.mensaje = e.Message;
            }
            return r;
        }

        public Boolean ExisteUserName(String username) {
            personas tmep = new personas();

            using (nomeEntities dbo = new nomeEntities()) {
                tmep = dbo.personas.FirstOrDefault(u => u.userName == username);
                if (tmep != null) {
                    return true; //no existe ese usuario
                } else {
                    return false;
                }
            }
        }

    }
}
