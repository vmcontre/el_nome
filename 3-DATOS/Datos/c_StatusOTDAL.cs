﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos {
    public class StatusOTDAL {
        //los status de las OT no los vamos a modificar ni crear. Está declarados en la DB (en el script de creación de la DB) por lo tanto, sólo los listaré y buscaré el nombre según el ID
        public List<ItemStatusOT> listarStatusOT() {
            List<ItemStatusOT> lista = new List<ItemStatusOT>();
            using (nomeEntities dbo = new nomeEntities()) {
                lista = (from l in dbo.statusOT select new ItemStatusOT { idStatusOT = l.idStatusOT, descrStatus=l.DescrStatus}).ToList();
            }
            return lista;
        }

        public String BuscarStatusOT(int id) {
            statusOT objStatusOT = new statusOT();
            String descripcion = "";
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objStatusOT = dbo.statusOT.FirstOrDefault(u => u.idStatusOT == id);
                    descripcion = objStatusOT.DescrStatus;
                }
            } catch  {
                descripcion = "";
            }
            return descripcion;
        }

        public int BuscarIDStatusOT(String descrStatus) {
            statusOT objStatusOT = new statusOT();
            int idStatus;
            try {
                using (nomeEntities dbo = new nomeEntities()) {
                    objStatusOT = dbo.statusOT.FirstOrDefault(u => u.DescrStatus == descrStatus);
                    idStatus = objStatusOT.idStatusOT;
                }
            } catch {
                idStatus = 0;
            }
            return idStatus;
        }
    }
}
