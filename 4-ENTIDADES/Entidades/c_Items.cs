﻿using System;

namespace Entidades {
    public class ItemOT {
        public int idOT { get; set; }
        public DateTime fechaCreacion { get; set; }
        public int idMaquina { get; set; }
        public int idCreador { get; set; }
        public int idEncargado { get; set; }
        public int idStatusOT { get; set; }
        public DateTime? fechaCierre { get; set; }
    }

    public class ItemDetalleOT {
        public int idDOT { get; set; }
        public int idOT { get; set; }
        public int idTecnico { get; set; }
        public int idRepuesto { get; set; }
        public int idFalla { get; set; }
        public DateTime fecha { get; set; }
    }

    public class ItemRepuestos {
        public int idRepuesto { get; set; }
        public String descrRepuesto { get; set; }
        public int stockRepuesto { get; set; }
    }
    public class ItemFallas {
        public int idFalla { get; set; }
        public String nombreFalla { get; set; }
        public String descrFalla { get; set; }
        public String sugerenciaFalla { get; set; }
    }

    public class ItemStatusOT {
        public int idStatusOT { get; set; }
        public String descrStatus { get; set; }
    }

    public class ItemMaquinas {
        public int idMaquina { get; set; }
        public String descrMaquina { get; set; }
        public DateTime fechaCompra { get; set; }
        public DateTime? fechaFinGtia { get; set; }
        public Boolean activo { get; set; }
    }

    public class ItemPersonas {
        public int idPersona { get; set; }
        public String nombrePersona { get; set; }
        public int idCargo { get; set; }
        public String nombreCargo { get; set; }
        public Boolean activo { get; set; }
        public String username { get; set; }
        public String passwd { get; set; }

    }

    public class ItemCargos {
        public int idCargo { get; set; }
        public String descrCargo { get; set; }
    }


}
