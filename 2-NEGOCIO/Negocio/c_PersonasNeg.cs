﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class PersonasNeg {
        private PersonasDAL personasDAL = new PersonasDAL();

        public ResultadoDTO CrearPersona(personas dto) {
            return personasDAL.CrearPersona(dto);
        }
        public ResultadoDTO CambiaEstadoPersona(int idPersona) {
            return personasDAL.CambiaEstadoPersona(idPersona);
        }
        public ResultadoDTO ModificarPersona(int idPersona, String nombrePersona, String UserName, int idCargo) {
            return personasDAL.ModificarPersona(idPersona, nombrePersona, UserName, idCargo);
        }
        public List<ItemPersonas> ListarPersonas() {
            return personasDAL.ListarPersonas();
        }

        public Boolean ValidaLogin(String username) {
            return personasDAL.ValidaLogin(username);
        }
        public ItemPersonas BuscarID(int idUsuario) {
            return personasDAL.BuscarID(idUsuario);
        }
        public ResultadoDTO CambiarContraseña(int idPersona, String nuevaPasswd) {
            return personasDAL.CambiarContraseña(idPersona, nuevaPasswd);
        }
        public ItemPersonas Buscar(String usuario, String clave) {
            return personasDAL.Buscar(usuario, clave);
        }
        public Boolean ExisteUserName(String username) {
            return personasDAL.ExisteUserName(username);
        }
    }
}
