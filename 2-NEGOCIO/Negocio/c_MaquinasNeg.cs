﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class MaquinasNeg {
        private MaquinasDAL maquinasDAL = new MaquinasDAL();

        public ResultadoDTO CrearMaquina(maquinas dto) {
            return maquinasDAL.CrearMaquina(dto);
        }
        public List<ItemMaquinas> ListarMaquinas() {
            return maquinasDAL.ListarMaquinas();
        }
        public ResultadoDTO ModificarMaquina(int idMaq, String nvaDescr, DateTime? nuevaFechaGtia) {
            return maquinasDAL.ModificarMaquina(idMaq, nvaDescr, nuevaFechaGtia);
        }
        public ResultadoDTO CambiaEstadoMaquina(int idMaq) {
            return maquinasDAL.CambiaEstadoMaquina(idMaq);
        }

        public ItemMaquinas BuscarMaquina(int idMaq) {
            return maquinasDAL.BuscarMaquina(idMaq);
        }
    }
}
