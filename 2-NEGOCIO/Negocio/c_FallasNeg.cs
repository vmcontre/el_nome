﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidades;

namespace Negocio {
    public class FallasNeg {
        private FallasDAL fallasDAL = new FallasDAL();

        public ResultadoDTO CrearFalla(fallas dto) {
            return fallasDAL.CrearFalla(dto);
        }
        public List<ItemFallas> ListarFallas() {
            return fallasDAL.ListarFallas();
        }
        public ResultadoDTO ModificarFallas(int idFalla, String descripcion, String sugerencia) {
            return fallasDAL.ModificarFallas(idFalla, descripcion, sugerencia);
        }
        public fallas BuscarFalla(int idFalla) {
            return fallasDAL.BuscarFalla(idFalla);
        }

    }
}
